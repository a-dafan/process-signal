//这个进程用于接收signal进程信号

#include <iostream>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>

using namespace std;

void handler(int signal)//进程接收到signal信号后不一定立即调用handler 因为CPU运行快 所以肉眼看着像立即调用
{
    printf("进程捕捉到了一个信号 信号编号为%d\n", signal);
}

int main()
{
    signal(2, handler);
    while (true)
    {
        cout << "我是一个进程： " << getpid() << endl;
        sleep(1);
    }
    return 0;
}