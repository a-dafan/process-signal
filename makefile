.PHONY:all
all:signal test
signal:signal.cc
	g++ -o $@ $^ -std=c++11 -g
test:test.cc
	g++ -o $@ $^ -std=c++11

.PHONY:clean
clean:
	rm -f signal test